import os


KEYWORDS = ['data', 'data scientist', 'python']

URL = 'https://jobs.ge/en/?page=1&q=&cid=6&lid=&jid='

CURRENT_PATH = os.getcwd()

JOB_LINK = 'https://jobs.ge/'

JOB_LIST_CLASS_NAME = "job_list_table"
DEADLINE_NUM_TD = 5
TITLE_NUM_TD = 1
COMPANY_NUM_TD = 3

NEW_JOB_MESSAGE_TITLE = 'NEW JOB'
NEW_JOB_MESSAGE = 'You have new unvisited job vacancy! Would you like to check it out?'
DEADLINE = 'deadline'
TITLE = 'title'
COMPANY = 'company'
LINK = 'link'

TODAY_DATA_FILE = os.path.join(CURRENT_PATH, 'data_today.json')
ALL_DATA_FILE = os.path.join(CURRENT_PATH, 'data_all.json')
LAST_JOB_DATA_FILE = os.path.join(CURRENT_PATH, 'last_job.json')

MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
          'August', 'September', 'October', 'November', 'December']