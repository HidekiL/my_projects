from datetime import datetime
from globals import *
from file_handling import JobJsonFile
from data_handling import search, get_first_job
from notification import popup


def main():
    all_data_file = JobJsonFile(ALL_DATA_FILE)
    today_data_file = JobJsonFile(TODAY_DATA_FILE)
    last_job_data_file = JobJsonFile(LAST_JOB_DATA_FILE)

    all_data_file.create_file()
    today_data_file.create_file()
    last_job_data_file.create_file()

    last_job = last_job_data_file.get_all_data()
    new_data = search(last_job)

    all_data_file.add_data(new_data)
    today_data_file.create_data(new_data)

    last_job = get_first_job()
    last_job_data_file.create_data(last_job)

    popup(new_data)

    all_data_file.delete_partial_data()


if __name__ == '__main__':
    main()

    date = datetime.now()
    print(f'{MONTHS[date.month - 1]} {date.day}, {date.year} -- executed successfully!')
