import requests

from bs4 import BeautifulSoup as bs
from globals import URL, TITLE, COMPANY, LINK, KEYWORDS, DEADLINE_NUM_TD, TITLE_NUM_TD, COMPANY_NUM_TD, \
    JOB_LINK, JOB_LIST_CLASS_NAME


# gets jobs whose titles match specific keywords
def in_interest(title):
    for keyword in KEYWORDS:
        if keyword in title.lower():
            return True
    return False


# returns job info
def get_job_info(element):
    try:
        job_deadline = element[DEADLINE_NUM_TD].text.strip()
        job_title = element[TITLE_NUM_TD].find('a').text.strip()
        job_company = element[COMPANY_NUM_TD].find('a').text.strip()
        job_link = JOB_LINK + element[TITLE_NUM_TD].find('a', href=True)['href']
        return [job_deadline, job_title, job_company, job_link]
    except IndexError as e:
        return None
    except AttributeError as s:
        return None


# get first data from job list
def get_first_job():
    r = requests.get(URL)

    soup = bs(r.text, 'html.parser')
    job_list = soup.find('table', {"id": JOB_LIST_CLASS_NAME})

    for el in job_list.find_all('tr'):
        job_info = el.find_all('td')
        if get_job_info(job_info) is None:
            continue
        else:
            return get_job_info(job_info)


# return all new job vacancies in dictionary
def search(last_job):
    r = requests.get(URL)

    soup = bs(r.text, 'html.parser')
    job_list = soup.find('table', {"id": JOB_LIST_CLASS_NAME})

    data_dict = {}

    for tr in job_list.find_all('tr'):
        job_info = tr.find_all('td')

        try:
            job_deadline, job_title, job_company, job_link = get_job_info(job_info)
        except TypeError:
            continue

        # check if jobs list was updated since last run of program
        if [job_deadline, job_title, job_company, job_link] == last_job:
            break

        if not in_interest(job_title):
            continue

        if job_deadline in data_dict:
            data_dict[job_deadline].append({TITLE: job_title,
                                            COMPANY: job_company,
                                            LINK: job_link})
        else:
            data_dict[job_deadline] = [{TITLE: job_title,
                                        COMPANY: job_company,
                                        LINK: job_link}]

    return data_dict
