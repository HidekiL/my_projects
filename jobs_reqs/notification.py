import webbrowser
import subprocess

from globals import NEW_JOB_MESSAGE_TITLE, NEW_JOB_MESSAGE, LINK
from tkinter import messagebox, Tk


# opens new job vacancies in default browser
def open_jobs(lnks):
    for lnk in lnks:
        webbrowser.open(lnk)


# gives notification to the user
def get_notification(data):
    if data:
        subprocess.call(['notify-send', NEW_JOB_MESSAGE_TITLE, NEW_JOB_MESSAGE])


# popup window, if user enters yes it will open job vacancies in browser otherwise do nothing
def popup(new_data):
    if new_data:
        links = []
        for d in new_data.values():
            for i in d:
                links.append(i[LINK])

        window = Tk()
        window.eval('tk::PlaceWindow %s center' % window.winfo_toplevel())
        window.withdraw()

        if messagebox.askyesno(NEW_JOB_MESSAGE_TITLE, NEW_JOB_MESSAGE):
            open_jobs(links)
        else:
            pass

        window.deiconify()
        window.destroy()
        window.quit()
