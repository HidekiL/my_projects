import json
import os

from datetime import datetime
from globals import MONTHS


class File:
	def __init__(self, file):
		self.file = file

	# Creates empty file if not exists
	def create_file(self):
		if self.not_exists():
			with open(self.file, 'w') as f:
				pass

	# returns true if file doesn't exist
	def not_exists(self):
		return not self.exists()

	# returns true if file is not empty
	def is_not_empty(self):
		return not self.is_empty()

	# deletes file
	def delete_file(self):
		os.remove(self.file)

	# returns true if file is empty
	def is_empty(self):
		return os.path.getsize(self.file) == 0

	# return true if file exists
	def exists(self):
		return os.path.exists(self.file)

	# returns date in format 'day-number month-full-name', todays date if nothing is passed
	@staticmethod
	def get_formatted_date(date=datetime.now()):
		return f'{str(date.day)} {MONTHS[date.month - 1]}'


class JobJsonFile(File):
	def __init__(self, file):
		super().__init__(file)

	# returns all data from file
	def get_all_data(self):
		if self.exists() and self.is_not_empty():
			with open(self.file, 'r') as f:
				return json.load(f)
		else:
			return {}

	# gets data which jobs deadline has not passed
	def get_partial_data(self, deadline_dates=None):
		if deadline_dates is None:
			deadline_dates = []

		if self.exists() and self.is_not_empty():
			with open(self.file, 'r') as f:
				data = json.load(f)

			for deadline_date in deadline_dates:
				try:
					del data[deadline_date]
				except KeyError:
					pass

			return data
		else:
			return {}

	# when file is empty, creates data
	def create_data(self, data):
		with open(self.file, 'w') as f:
			json.dump(data, f)

	# adds data to file
	def add_data(self, new_data):
		if self.not_exists() or self.is_empty():
			self.create_data(new_data)
		else:
			data = self.get_all_data()
			for deadline in new_data.keys():
				if deadline in data.keys():
					data[deadline].append(new_data[deadline])
				else:
					data[deadline] = [{new_data[deadline]}]
			self.delete_all_data()
			self.create_data(data)

	# deletes jobs which deadline is passed
	def delete_partial_data(self):
		new_data = self.get_partial_data(deadline_dates=[self.get_formatted_date()])
		self.delete_all_data()
		self.create_data(new_data)

	# deletes all data from file
	def delete_all_data(self):
		self.delete_file()
		self.create_file()


class JobCsvFile(File):
	def __init__(self, file):
		super().__init__(file)