"""
Find expected value of game darts and bullseye in 2D.

Description:
https://www.youtube.com/watch?v=6_yU9eJ0NxA&t=477s
"""


from random import random
import math


def in_circle(x, y, r):
	return (x-0.5)**2 + (y-0.5)**2 < r**2


def update_radius(x, y, r):
	height = math.sqrt((x-0.5)**2 + (y-0.5)**2)
	return math.sqrt(r**2 - height**2)


def play_game():
	radius = 0.5
	point = 1

	while True:
		x, y = round(random(), 4), round(random(), 4)
		if not in_circle(x, y, radius):
			break

		point += 1
		radius = round(update_radius(x, y, radius), 4)

	return point


def expected_value(n):
	points = []
	for i in range(n):
		points.append(play_game())

	expect_val = 0
	for i in range(n):
		expect_val += points[i] 

	return expect_val / n


if __name__ == '__main__':
	num_of_games = 100000
	print(expected_value(num_of_games))
