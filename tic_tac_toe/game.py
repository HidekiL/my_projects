import pygame
import sys
import os

from pygame.locals import QUIT
from board import Board, display_winner
from player import Player
pygame.init()

DISPLAY_SURFACE = pygame.display.set_mode((300, 300))
pygame.display.set_caption('Tic Tac Toe')

p1 = Player('x', os.path.join(os.getcwd(), 'img/x.png'))
p2 = Player('o', os.path.join(os.getcwd(), 'img/o.png'))
p1.turn = True

board = Board(300, 300)


def main():
    while True:
        board.draw(DISPLAY_SURFACE, p1.img, p2.img)
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                if p1.turn:
                    p1.turn, p2.turn = board.update(p1.sign, pos)
                else:
                    p2.turn, p1.turn = board.update(p2.sign, pos)

                winner, game_over = board.game_over()
                if game_over:
                    display_winner(DISPLAY_SURFACE, winner)
                    board.refresh()

            pygame.display.update()


if __name__ == '__main__':
    main()
