import pygame
import os
import math
import time


def display_winner(surface, winner):
    t = time.time()
    while time.time() - t <= 2:
        surface.fill((255, 255, 255))
        font = pygame.font.Font('freesansbold.ttf', 32)

        if winner == 'x':
            text = font.render('Player 1 wins!', True, (0, 255, 0), (0, 0, 128))
            surface.blit(text, (20, 140))
        else:
            text = font.render('Player 2 wins!', True, (0, 255, 0), (0, 0, 128))
            surface.blit(text, (20, 140))
            
        pygame.display.update()


class Board:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.board_pos = (0, 0)
        self.board = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        self.img = self.board_image()

    def board_image(self):
        img = pygame.image.load(os.path.join(os.getcwd(), 'img/board.png'))
        img = pygame.transform.scale(img, (self.width, self.height))
        return img

    def draw(self, surface, x_img, o_img):
        surface.fill((255, 255, 255))
        surface.blit(self.img, (self.board_pos[0], self.board_pos[1]))

        img_coord = self.board_pos[0] + 30
        for x in range(len(self.board)):
            for y in range(len(self.board[0])):
                if self.board[x][y] == 'o':
                    surface.blit(o_img, (img_coord + x*100, img_coord + y*100))
                elif self.board[x][y] == 'x':
                    surface.blit(x_img, (img_coord + x*100, img_coord + y*100))
                else:
                    continue

    def update(self, player, pos):
        x, y = self.get_board_coord(pos)
        if self.board[x][y] not in ['o', 'x']:
            self.board[x][y] = player
            return False, True

        return True, False

    def get_board_coord(self, pos):
        return math.floor((pos[0] - self.board_pos[0]) / 100), math.floor((pos[1] - self.board_pos[1]) / 100)

    def game_over(self):
        # check for diagonals
        if self.board[0][0] == self.board[1][1] == self.board[2][2]:
            return self.board[0][0], True

        if self.board[0][2] == self.board[1][1] == self.board[2][0]:
            return self.board[0][2], True

        # check horizontally
        if self.board[0][0] == self.board[0][1] == self.board[0][2]:
            return self.board[0][0], True

        if self.board[1][0] == self.board[1][1] == self.board[1][2]:
            return self.board[1][0], True

        if self.board[2][0] == self.board[2][1] == self.board[2][2]:
            return self.board[2][0], True

        # check vertically
        if self.board[0][0] == self.board[1][0] == self.board[2][0]:
            return self.board[0][0], True

        if self.board[0][1] == self.board[1][1] == self.board[2][1]:
            return self.board[0][1], True

        if self.board[0][2] == self.board[1][2] == self.board[2][2]:
            return self.board[0][2], True

        return None, False

    def refresh(self):
        self.board = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]