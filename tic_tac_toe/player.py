import pygame


class Player:
    def __init__(self, sign, path):
        self.turn = False
        self.path = path
        self.sign = sign
        self.width = 40
        self.height = 40
        self.img = self.get_image()

    def get_image(self):
        img = pygame.image.load(self.path)
        img = pygame.transform.scale(img, (self.width, self.height))
        return img

