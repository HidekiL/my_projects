"""
Generates folder with files in daily_coding folder:

folder_name: problem_#<num> where <num> is next problem number which was not found.
files: sol.py, my_file.py, test_my_file.py with some useful predefined functions and classes in it
"""


import os
import re


def get_next_problem_number(pth):
	os.chdir(pth)

	problem_number = 0

	for filename in os.listdir(pth):
		try:
			curr_problem_num = int(re.findall('\d+', filename)[0])
		except IndexError:
			continue

		if curr_problem_num > problem_number:
			problem_number = curr_problem_num

	return str(problem_number+1)


def generate_test_text(problem, test_file, file, function):
	with open(test_file, 'r') as f:
		text = f.read()

	text = text.replace('<file_name>', file).replace('<function_name>', function).replace('<c_function_name>', function.capitalize())
	return text


def generate_main_file_text(function):
	text = '"""\n' + 'description goes here' + '\n"""\n\n\n' 
	text += 'def ' + function + '():\n\tpass'
	return text


def generate(pth, folder, main_file, test_file, sol_file, main_file_txt, test_txt):
	os.mkdir(folder)
	os.chdir(pth + '/' + folder)

	with open(main_file+'.py', 'w') as main_f, open(test_file+'.py', 'w') as test_f, open(sol_file+'.py', 'w') as sol_f:
		main_f.write(main_file_txt)
		test_f.write(test_txt)


if __name__ == '__main__':
	my_file_name = input('Enter file name: ')
	test_my_file_name = 'test_' + my_file_name
	sol_file_name = 'sol'

	main_function_name = input('Enter main function name: ')
	
	test_case_file_name = 'test_case.txt'

	path = '/home/hp/programming/daily_coding/my_solutions'
	folder_name = 'problem_#' + get_next_problem_number(path)
	os.chdir('/home/hp/programming/my_projects/gen_dl_code')
	test_text = generate_test_text(folder_name, test_case_file_name, my_file_name, main_function_name)
	main_file_text = generate_main_file_text(main_function_name)

	os.chdir('/home/hp/programming/daily_coding/my_solutions')
	generate(path, folder_name, my_file_name, test_my_file_name, sol_file_name, main_file_text, test_text)