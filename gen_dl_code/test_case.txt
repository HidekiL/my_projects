import random 
import unittest

from <file_name> import <function_name> as f 


class Test<c_function_name>(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		pass

	def test_my_example(self):
		pass
		
	def test_empty(self):
		pass
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()