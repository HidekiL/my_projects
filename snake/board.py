import pygame
import time

from globals import *


# draws board
def draw(apple, snake, surface):
    for x in range(GRID_HEIGHT):
        for y in range(GRID_WIDTH):
            if (x, y) in snake:
                pygame.draw.rect(surface, GREEN, [(MARGIN + SQUARE_WIDTH) * x + MARGIN,
                                                  (MARGIN + SQUARE_HEIGHT) * y + MARGIN,
                                                  SQUARE_WIDTH,
                                                  SQUARE_HEIGHT])
            elif (x, y) == apple:
                pygame.draw.rect(surface, RED, [(MARGIN + SQUARE_WIDTH) * x + MARGIN,
                                                (MARGIN + SQUARE_HEIGHT) * y + MARGIN,
                                                SQUARE_WIDTH,
                                                SQUARE_HEIGHT])
            else:
                pygame.draw.rect(surface, BLACK, [(MARGIN + SQUARE_WIDTH) * x + MARGIN,
                                                  (MARGIN + SQUARE_HEIGHT) * y + MARGIN,
                                                  SQUARE_WIDTH,
                                                  SQUARE_HEIGHT])
    pygame.display.update()


# draws game over board
def game_over(surface, point):
    surface.fill(WHITE)
    font = pygame.font.Font(None, GAME_OVER_FONT_SIZE)
    text = font.render(f"GAME OVER! You earned {point} points", True, BLACK)
    text_rect = text.get_rect(center=(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2))
    surface.blit(text, text_rect)
    pygame.display.update()
    time.sleep(2)
