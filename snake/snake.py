from collections import deque
from random import randint
from globals import *


class Snake:
    def __init__(self):
        self.snake_form = deque()
        self.points = 0
        self.tail = None
        self.head = self.create_snake()
        self.fps = STARTING_FRAME

    # create snake
    def create_snake(self):
        x, y = randint(MARGIN_FROM_WALL, GRID_HEIGHT-MARGIN_FROM_WALL), \
               randint(MARGIN_FROM_WALL, GRID_WIDTH - MARGIN_FROM_WALL)
        self.snake_form.append((x, y))
        return x, y

    # update snake
    def update(self, move, apple):
        if move == RIGHT:
            self.tail = self.snake_form.popleft()
            self.snake_form.append((self.head[0] + 1, self.head[1]))
            self.head = (self.head[0] + 1, self.head[1])
        elif move == LEFT:
            self.tail = self.snake_form.popleft()
            self.snake_form.append((self.head[0] - 1, self.head[1]))
            self.head = (self.head[0] - 1, self.head[1])
        elif move == DOWN:
            self.tail = self.snake_form.popleft()
            self.snake_form.append((self.head[0], self.head[1] + 1))
            self.head = (self.head[0], self.head[1] + 1)
        elif move == UP:
            self.tail = self.snake_form.popleft()
            self.snake_form.append((self.head[0], self.head[1] - 1))
            self.head = (self.head[0], self.head[1] - 1)

        if self.head == apple:
            self.snake_form.appendleft(self.tail)

            return True

    # checks if snake is dead
    def is_dead(self):
        if self.head[0] < 0 or self.head[0] >= GRID_HEIGHT or self.head[1] < 0 or self.head[1] >= GRID_WIDTH:
            return True

        for i in range(len(self.snake_form)-1):
            if self.snake_form[i] == self.head:
                return True

    # update point
    def update_points(self, fruit):
        self.points += fruit.point
