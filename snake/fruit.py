import pygame
import random

from globals import *


class Apple:
    def __init__(self, point):
        self.point = point
        self.x = None
        self.y = None

    # creates new fruit
    def create_new_fruit(self, snake):
        self.x, self.y = random.randint(1, GRID_HEIGHT - 1), random.randint(1, GRID_WIDTH - 1)
        while (self.x, self.y) in snake:
            self.x, self.y = random.randint(1, GRID_HEIGHT - 1), random.randint(1, GRID_WIDTH - 1)