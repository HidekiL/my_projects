"""
SNAKE GAME
"""


import pygame
import sys
import random

from globals import *
from snake import Snake
from board import draw, game_over
from fruit import Apple
from pygame.locals import QUIT


# initiate pygame window
pygame.init()

DISPLAY_SURFACE = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption(GAME_NAME)
DISPLAY_SURFACE.fill(WHITE)

# initiate objects
snake = Snake()
apple = Apple(APPLE_POINT)
apple.create_new_fruit(snake.snake_form)


# Main game loop
def main():
    direction = random.choice([LEFT, RIGHT, UP, DOWN])
    clock = pygame.time.Clock()
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and direction != RIGHT:
                    direction = LEFT
                if event.key == pygame.K_RIGHT and direction != LEFT:
                    direction = RIGHT
                if event.key == pygame.K_DOWN and direction != UP:
                    direction = DOWN
                if event.key == pygame.K_UP and direction != DOWN:
                    direction = UP

        # updates snake form and checks if apple was eaten
        if snake.update(direction, (apple.x, apple.y)):
            apple.create_new_fruit(snake.snake_form)
            snake.update_points(apple)

        draw((apple.x, apple.y), snake.snake_form, DISPLAY_SURFACE)
        if snake.is_dead():
            game_over(DISPLAY_SURFACE, snake.points)
            break

        clock.tick(snake.fps)


if __name__ == '__main__':
    main()
